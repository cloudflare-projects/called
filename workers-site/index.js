import { getAssetFromKV } from '@cloudflare/kv-asset-handler'

addEventListener('fetch', event => {
  event.respondWith(handleRequest(event))
})

async function handleRequest(event) {
  const url = new URL(event.request.url)
  if (url.pathname == '/json'){return await returnData(url.host);}
  return await returnAsset(event);
}

async function returnData(host){
  let key = host.split('.').slice(0,2).join('.')
  let data = await CALLED.get(key);
  console.log(key, data);
  return new Response(data, {headers: {'content-type' : 'application/json'}});
}


async function returnAsset(event) {
  const page = await getAssetFromKV(event, {})
  const response = new Response(page.body, page)
  response.headers.set('X-XSS-Protection', '1; mode=block')
  response.headers.set('X-Content-Type-Options', 'nosniff')
  response.headers.set('X-Frame-Options', 'DENY')
  response.headers.set('Referrer-Policy', 'unsafe-url')
  response.headers.set('Feature-Policy', 'none')
  return response
}
